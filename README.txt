# Picasa Tab - Integrate the picasa image selector into your site

## Description--

 -Picasa Tab lets you connect your site from your picasa albums using your username.  

This module lets you Adds a 'Picasa album' tab to the media dialog so that you can easily browse images from  the Media Selector widget.

## Installation--  

Install as you would any other Drupal module:

1. Download the module and put it in sites/all/modules or sites/SITENAME/modules .
3. Go to admin/config/media/picasa and enter your picasa username.
4. Enable media field module and add picasa in your media field settings under "Enabled browser plugins".
