/**
 * @file
 * This file handles the JS for Picasa Tab Module functions.
 */

/**
 * Save selected picasa image data.
 */

function saveData(obj) {
  var src = jQuery(obj).attr('alt');
  var values = new Array();
  var now = jQuery.now();

  photoNameArr = src.split('/');
  photoNameArr.reverse();
  extArr = photoNameArr[0].split('.');
  values = {
    url: src,
    filename: photoNameArr[0],
    mimetype: 'image/' + extArr[1],
    isWriteable: 'true'
  };

  if (jQuery(obj).hasClass('img-select')) {
    jQuery(obj).removeClass('img-select');
    jQuery('#picasa-files').val('');
  }
  else {
    jQuery('.img-select').removeClass('img-select');
    jQuery(obj).addClass('img-select');
    jQuery('#picasa-files').val(JSON.stringify(values));
  }
}
